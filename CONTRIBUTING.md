## Ways to contribute

Here are the best ways in which you can help the project:

1. [Donate](https://www.patreon.com/bePatron?u=65739770)
2. [Help with existing issues](https://gitlab.com/zehkira/myuzi/-/issues/?label_name%5B%5D=help%20wanted)
3. [Report bugs](https://gitlab.com/zehkira/myuzi/-/issues/new)

## Issues

When reporting a bug, include the following information:
- Your operating system (e.g Ubuntu 22.04)
- The installation method you used (e.g. Flatpak)
- The version of the app (e.g. 1.2.3)

Avoid creating duplicate issues.

## Merge requests

Before starting work on a significant change, open an issue and explain what you want to do. This can prevent you from wasting time on a change that would not be accepted anyway.

You **must** test your code before submitting it. Your code **must** work with Python 3.9, as that is what Flatpak uses.

### Code style

- Tabs
- Single quotes `'` everywhere
- CamelCase for class names, snake_case for everything else
- Basic type hints (without typing module)
